<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" encoding="utf-8" />

    <xsl:template match="/">
        <fo:root font-family="serif" font-size="14px" language="eng" color="#222">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="cover" page-height="297mm" page-width="210mm" margin="20mm">
                    <fo:region-body margin-top="100mm" />
                    <fo:region-after extent="5mm" />
                </fo:simple-page-master>
                <fo:simple-page-master master-name="page" page-height="297mm" page-width="210mm" margin="20mm">
                    <fo:region-body margin-bottom="10mm" />
                    <fo:region-after extent="5mm" />
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="cover">
                <fo:flow flow-name="xsl-region-body">
                    <fo:block text-align="center" font-size="3em" margin-bottom="10mm">
                        Random countries
                    </fo:block>
                    <fo:block text-align="center">
                        Made by <xsl:value-of select="/project/author/name" /> (<xsl:value-of select="/project/author/email" /> ) as a BI-XML semestral work.
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>


            <fo:page-sequence master-reference="page">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center" font-size="0.75em">
                        <fo:page-number />
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="2.1em" margin-bottom="10mm">
                        <fo:inline>Index</fo:inline>
                    </fo:block>

                    <xsl:for-each select="/project/countries/country">
                        <fo:block margin-bottom="5mm" text-align-last="justify">
                            <xsl:variable name="cname" select="@name" />
                            <fo:inline>
                                <fo:basic-link internal-destination="{$cname}">
                                    <xsl:value-of select="@name" />
                                    <fo:leader leader-pattern="dots" />
                                    <fo:page-number-citation ref-id="{$cname}" />
                                </fo:basic-link>
                            </fo:inline>
                        </fo:block>
                    </xsl:for-each>

                    <xsl:apply-templates select="/project/countries/country" />
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="/project/countries/country">
        <fo:block text-align="center" page-break-before="always" font-size="2.1em" font-weight="900" id="{./@name}">
            <xsl:value-of select="@name"/>
        </fo:block>
        <xsl:apply-templates select="images"/>
        <xsl:apply-templates select="section"/>
    </xsl:template>

    <xsl:template match="images">
        <fo:block text-align="left" font-size="1.7em" id="{../@name}/images">
            <xsl:value-of select="@name"/>
        </fo:block>
        <xsl:for-each select="image">
            <fo:block text-align="center">
                <fo:external-graphic content-height="scale-to-fit" content-width="3.00in">
                    <xsl:attribute name="src">
                        <xsl:value-of select="@source"/>
                    </xsl:attribute>
                </fo:external-graphic>
            </fo:block>
            <fo:block margin-bottom="3mm" margin-top="3mm">
                <xsl:value-of select="@caption"/>
            </fo:block>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="section">
        <fo:block page-break-before="always" font-size="2em" font-weight="900" id="{../@name}/{./@name}">
            <xsl:value-of select="@name"/>
        </fo:block>
        <fo:block>
            <xsl:apply-templates select="data"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="data">
        <fo:block font-size="1.3em" margin-top="3.5mm">
            <fo:inline font-weight="700"><xsl:value-of select="@name" /></fo:inline>
        </fo:block>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="numberNumeric">
        <xsl:apply-templates/>
        <fo:inline><xsl:value-of select="@unit"/></fo:inline>

    </xsl:template>

    <xsl:template match="numberDecimal">
        <xsl:apply-templates/>
        <fo:inline><xsl:value-of select="@unit"/></fo:inline>
    </xsl:template>

</xsl:stylesheet>