<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" name="html" omit-xml-declaration="yes" indent="yes" encoding="utf-8" media-type="text/html" doctype-public="-//W3C//DTD HTML 4.0//EN" />

    <xsl:template match="/">
        <xsl:result-document href="index.htm" method="html" encoding="UTF-8" indent="yes">
            <html>
                <head>
                    <meta charset='utf-8' />
                    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
                    <title>Countries | BI-XML</title>
                    <meta name='viewport' content='width=device-width, initial-scale=1' />
                    <link rel='stylesheet' type='text/css' media='screen' href='style.css' />
                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
                </head>

                <body>
                    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="color : #DB7093">
                        <a class="navbar-brand" href="../html/index.htm">Countries</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <xsl:for-each select="/project/countries/country">
                                    <li class="nav-item">
                                        <a class="nav-link">
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="concat('country/', translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '.htm')" />
                                            </xsl:attribute>
                                            <xsl:value-of select="@name" />
                                        </a>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </div>
                    </nav>
                    <main class="py-5" style="background-color: #f4cccc;">
                        <div class="container">
                            <div class="row">
                                <div class="display-2">Hello, this is my BI-XML project</div>
                            </div>
                            <div class="row">
                                <div class="lead desctiprion">
                                    You can view information about 6 countries
                                    by clicking on appropriate link in the top navbar or on block below.
                                </div>
                                <div class="row mt-5 mb-5">
                                    <xsl:for-each select="/project/countries/country">
                                        <div class="col-4">
                                            <div class="card mb-5 mx-auto" style="width: 20rem">
                                                <img class="card-img-top" alt="photo of country">
                                                    <xsl:attribute name="src">
                                                        <xsl:value-of select="images/image[1]/@source" />
                                                    </xsl:attribute>
                                                </img>
                                                <div class="card-body">
                                                    <h5 class="card-title">
                                                        <xsl:value-of select="@name" />
                                                    </h5>
                                                    <p class="card-text">
                                                        <xsl:value-of select="images/image[1]/@caption" />

                                                    </p>
                                                    <a class="btn btn-primary">
                                                        <xsl:attribute name="href">
                                                            <xsl:value-of select="concat('country/', translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '.htm')" />
                                                        </xsl:attribute>
                                                        More
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:for-each>
                                </div>
                            </div>
                        </div>
                    </main>
                    <footer class="py-3 bg-$pink-200">
                        <div class="container">
                            Made by 
                            <xsl:value-of select="project/author/name" />
                            (
                                <xsl:value-of select="project/author/email" />
                            ) as a BI-XML semestral work.
                            <br />
                            Data source:
                            <a class="link" href="https://www.cia.gov/the-world-factbook/">CIA: The World Factbook</a>
                        </div>
                    </footer>
                </body>
            </html>
        </xsl:result-document>

        <xsl:for-each select="project/countries/country">
            <xsl:variable name="lettercase" select="translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
            <xsl:variable name="filename"
                select="concat('country/', $lettercase,'.htm')" />
            <xsl:result-document href="{$filename}" format="html">
                <html>
                    <head>
                        <meta charset='utf-8' />
                        <meta http-equiv='X-UA-Compatible' content='IE=edge' />
                        <title>
                            <xsl:value-of select="@name" />
                            | BI-XML
                        </title>
                        <meta name='viewport' content='width=device-width, initial-scale=1' />
                        <link rel='stylesheet' type='text/css' media='screen' href='../style.css' />
                        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
                            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
                    </head>

                    <body>
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                            <a class="navbar-brand" href="../index.htm">Countries</a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto">
                                    <xsl:for-each select="/project/countries/country">
                                        <li class="nav-item">
                                            <a class="nav-link">
                                                <xsl:attribute name="href">
                                                    <xsl:value-of select="concat('../country/', translate(@name, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '.htm')" />
                                                </xsl:attribute>
                                                <xsl:value-of select="@name" />
                                            </a>
                                        </li>
                                    </xsl:for-each>
                                </ul>
                            </div>
                        </nav>

                        <main class="py-5" style="background-color: #f4cccc;">
                            <div class="container">
                                <div class="row title">
                                    <div class="display-2">
                                        <xsl:value-of select="@name" />
                                    </div>
                                </div>
                                <xsl:apply-templates select="images"/>
                                <xsl:apply-templates select="section" />
                            </div>
                        </main>


                        <footer class="py-3 bg-light">
                            <div class="container">
                                Made by 
                                <xsl:value-of select="/project/author/name" />
                                    (
                                        <xsl:value-of select="/project/author/email" />
                                    ) as a BI-XML semestral work.
                                <br />
                                Data source:
                                <a class="link" href="https://www.cia.gov/the-world-factbook/">CIA: The World Factbook</a>
                            </div>
                        </footer>
                    </body>

                </html>
            </xsl:result-document>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="images">
        <div class="row py-5">
            <xsl:for-each select="image">
                <div class="col-3">
                    <img class="country-image">
                        <xsl:attribute name="src">
                            <xsl:value-of select="@source" />
                        </xsl:attribute>
                    </img>
                </div>
            </xsl:for-each>
            
        </div>
    </xsl:template>
    <xsl:template match="section">
        <div class="block" id="{@name}">
            <div class="row py-2">
                <div class="display-3"><xsl:value-of select="@name"/></div>
            </div>
            <xsl:for-each select="data">
                <div class="block">
                    <div class="row py-2">
                        <div class="display-4">
                            <xsl:value-of select="@name" />
                        </div>
                    </div>
                    <div class="row py-2">
                        <div class="lead">
                            <xsl:apply-templates/>
                        </div>
                    </div>
                </div>
            </xsl:for-each>
        </div>
        <hr />
    </xsl:template>

    <xsl:template match="numberNumeric">
        <xsl:apply-templates/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="@unit"/>
    </xsl:template>

    <xsl:template match="numberDecimal">
        <xsl:apply-templates/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="@unit"/>
    </xsl:template>

</xsl:stylesheet>