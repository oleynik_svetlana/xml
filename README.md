# BI-XML - Countries


- checking the final file using the DTD scheme: <br>
  <code>xmllint --noout --dtdvalid scheme/scheme.dtd data.xml</code>
- checking single files using the DTD scheme: <br>
  <code>xmllint --noout --dtdvalid scheme/scheme.dtd data/\*.xml</code>
- checking the final file using the RelaxNG scheme: <br>
  <code>xmllint --noout --relaxng scheme/scheme.rng data.xml</code>

You can use the command <code>make</code> 
<code>make html</code>
<code>make pdf</code>
