all: validate html pdf

validate:
	@echo "Validating via DTD..."
	xmllint --noout --dtdvalid scheme/scheme.dtd data/*.xml
	@if [ $$? -ne 0 ]; then @echo "Error! Exiting...\n"; exit; fi;
	@echo "OK"

	xmllint --noout --dtdvalid scheme/scheme.dtd data.xml
	@if [ $$? -ne 0 ]; then @echo "\nError! Exiting...\n"; exit; fi;
	@echo "OK"

	@echo "\nValidating via RelaxNG..."
	xmllint --noout --relaxng scheme/scheme.rng data.xml
	@if [ $$? -ne 0 ]; then @echo "\nError! Exiting...\n"; exit; fi;
	@echo "OK"

html:
	@echo "\nPreparing environment...\n"
	@rm -rf out/html
	@mkdir -p out/html
	@cp -r assets/* out/html

	@echo "Compiling...\n"
	java -jar lib/saxon/saxon-he-10.3.jar  -o:out/html/index.htm -s:data.xml -xsl:xslt/html.xslt

pdf:
	@echo "\nPreparing environment...\n"
	@rm -rf out/pdf
	@mkdir -p out/pdf

	@echo "Compiling...\n"
	java -jar lib/saxon/saxon-he-10.3.jar -o:out/pdf/output.fo -s:data.xml -xsl:xslt/pdf.xslt && lib/fop/fop -fo out/pdf/output.fo -pdf out/pdf/output.pdf
	@rm -rf out/pdf/output.fo

